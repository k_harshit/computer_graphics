//
// Created by harshit on 11/6/17.
//

#include <graphics.h>

void rotation_2d(double theta){
    int x1=200, y1=200, x2=300, y2=300;
    line(x1, y1, x2, y2);

    theta = theta * (3.14/180);
    x2 = x2*cos(theta) - y2*sin(theta);
    y2 = x2*sin(theta) + y2*cos(theta);

    setcolor(3);
    line(x1, y1, x2, y2);
}


void rotation_3d(double theta){
    int left = 200, top= 200, right=400, bottom=400, depth=50, topflag=1;
    bar3d(left, top, right, bottom, depth, topflag);

    theta = theta*(3.14/180);

    left = left*cos(theta) - top*sin(theta);
    top = left*sin(theta) + top*cos(theta);
    right = right*cos(theta) - bottom*sin(theta);
    bottom = right*sin(theta) + bottom*cos(theta);

    setcolor(3);
    bar3d(left, top, right, bottom, depth, topflag);
}