//
// Created by harshit on 11/4/17.
//

#include <graphics.h>

void BresenhamLine(int x0, int y0, int x1, int y1){

    int dx = abs(x1 - x0);
    int dy = abs(y1 - y0);

    if (dx == 0) {
        printf("dx can't be zero for Bresenham's line algorithm to work!");
        return;
    }

    int m = dy / dx;

    // For |m| < 1
    if (m < 1) {
        int d = 2*dy - dx;
        while (x0 < x1) {
            putpixel(x0, y0, BLUE);
            x0 += 1;
            if (d < 0)
                d += 2 * dy;
            else {
                d += 2 * (dy - dx);
                y0 += 1;
            }
            delay(100);
        }
    }
    // For |m| > 1
    else {
        int d = 2*dx - dy;
        while (y0 < y1) { // |m| < 1
            putpixel(x0, y0, BLUE);
            y0 += 1;
            if (d < 0)
                d += 2 * dx;
            else {
                d += 2 * (dx - dy);
                x0 += 1;
            }
            delay(100);
        }
    }
}
