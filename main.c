#include <stdio.h>
#include <graphics.h>

void wait_for_char();
void DDA(int x0, int y0, int x1, int y1);
void BresenhamLine(int x0, int y0, int x1, int y1);
void MidpointCircle(int x0, int y0, int r);
void BresenhamCircle(int x0, int y0, int r);
void scaling_2d(int sx, int sy);
void translation_2d(int tx, int ty);
void rotation_2d(double theta);
void translation_3d(int tx, int ty);
void scaling_3d(int sx, int sy);
void rotation_3d(double theta);
void CohenSutherlandLineClipAndDraw(double x0, double y0, double x1, double y1);

int main() {

    int gd = DETECT,gm,left=100,top=100;

    // Initialize graphics function
    initgraph(&gd, &gm, NULL);

    int x0 = 2, y0 = 2, x1 = 16, y1 = 14;
//    DDA(left + x0, top +  y0, left +  x1, top + y1);
//    BresenhamLine(2*left + x0, 2*top + y0, 2*left + x1, 2*top + y1);
//    MidpointCircle(3*left + x0, 3*top + y0, 12);
//    BresenhamCircle(4*left + x0, 4*top + y0, 12);

//    outtextxy (100,88,"Object.");
//    rectangle(100,250,150,200);
//    int tx = 100, ty = 30;
//    translation_2d(tx, ty);
//    int sx = 2, sy = 2;
//    scaling_2d(sx, sy);
//    rotation_2d(30);

//    bar3d(100, 100, 200, 200, 50, 1);
//    translation_3d(tx, ty);
//    scaling_3d(sx, sy);
//    rotation_3d(30);
    CohenSutherlandLineClipAndDraw(40, 20, 10, 70);

    wait_for_char();
    closegraph();


    return 0;
}


void wait_for_char() {

    //Wait for a key press
    int in = 0;

    while (in == 0) {
        in = getchar();
    }
}
