//
// Created by harshit on 11/4/17.
//

#include <graphics.h>

void pixel(int xc, int yc, int x, int y);


void MidpointCircle(int x0, int y0, int r){
    int x = 0, y = r;
    int p =  1 - r;
    pixel(x0, y0, x, y);

    while (x < y){
        if (p < 0){
            p += 2*x + 1;
        } else{
            y -= 1;
            p += 2*(x - y) + 1;
        }
        x += 1;
        pixel(x0, y0, x, y);
        delay(100);
    }
}


void pixel(int xc, int yc, int x, int y){
    putpixel(xc + x, yc + y, 7);
    putpixel(xc + y, yc + x, 7);
    putpixel(xc - y, yc + x, 7);
    putpixel(xc - x, yc + y, 7);
    putpixel(xc - x, yc - y, 7);
    putpixel(xc - y, yc - x, 7);
    putpixel(xc + y, yc - x, 7);
    putpixel(xc + x, yc - y, 7);
}