//
// Created by harshit on 11/6/17.
//

#include <graphics.h>

void translation_2d(int tx, int ty){
    setcolor(3);
    rectangle(100+tx, 250+ty, 150+tx, 200+ty);
}

void translation_3d(int tx, int ty){
    setcolor(5);
    bar3d(100+tx, 100+ty, 200+tx, 200+ty, 50, 1);
}
