cmake_minimum_required(VERSION 3.8)
project(computer_graphics)

set(CMAKE_C_STANDARD 99)

set(SOURCE_FILES main.c dda.c bresenham_line.c midpoint_circle.c bresenham_circle.c scaling.c translation.c rotation.c cohen_sutherland_line_clipping.c)
add_executable(computer_graphics ${SOURCE_FILES})

SET(CMAKE_EXE_LINKER_FLAGS "-lgraph -lm")
