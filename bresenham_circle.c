//
// Created by harshit on 11/4/17.
//

#include <graphics.h>

void pixel(int xc, int yc, int x, int y);

void BresenhamCircle(int x0, int y0, int r){
    int x = 0, y = r;
    int p = 3 - 2*r;
    pixel(x0, y0, x, y);

    while (y > x){
        if (p < 0)
            p += 4*x + 6;
        else{
            p += 4*(x - y) + 10;
            y--;
        }
        x += 1;
        pixel(x0, y0, x, y);
        delay(100);
    }
}

