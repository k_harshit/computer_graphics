//
// Created by harshit on 11/4/17.
//

#include <graphics.h>

void DDA(int x0, int y0, int x1, int y1){

    // calculate dx and dy
    int dx = x1 - x0;
    int dy = y1 - y0;

    // calculate steps required for generating pixels
    int steps = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    // calculate increment in x and y for each step
    float xinc = dx / (float) steps;
    float yinc = dy / (float) steps;

    // Put pixel for each step
    float x = x0;
    float y = y0;

    for (int i = 0; i < steps; ++i) {
        putpixel(x, y, RED);  // put pixel at (x, y)
        x += xinc;  // increment in x at each step
        y += yinc;
        delay(100);  // for visualization of line generation step by step
    }
}



