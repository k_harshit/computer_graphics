//
// Created by harshit on 11/6/17.
//

#include <graphics.h>

void scaling_2d(int sx, int sy){
    setcolor(2);
    outtextxy(240, 10, "SCALING");
    rectangle(100*sx, 150*sy, 150*sx, 200*sy);
}

void scaling_3d(int sx, int sy){
    setcolor(2);
    bar3d(100*sx, 100*sy, 200*sx, 200*sy, 50*sx, 1);
}